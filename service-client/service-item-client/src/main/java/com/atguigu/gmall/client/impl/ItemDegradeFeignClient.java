package com.atguigu.gmall.client.impl;

import com.atguigu.gmall.client.ItemFeignClient;
import com.atguigu.gmall.common.result.Result;
import org.springframework.stereotype.Component;

/**
 * @author mqx
 * @date 2020-11-9 10:39:51
 */
@Component
public class ItemDegradeFeignClient implements ItemFeignClient {
    @Override
    public Result getItem(Long skuId) {
        return null;
    }
}
