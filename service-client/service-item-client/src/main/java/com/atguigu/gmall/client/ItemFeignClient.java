package com.atguigu.gmall.client;

import com.atguigu.gmall.client.impl.ItemDegradeFeignClient;
import com.atguigu.gmall.common.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author mqx
 * @date 2020-11-9 10:39:13
 */
@FeignClient(value = "service-item",fallback = ItemDegradeFeignClient.class)
public interface ItemFeignClient {

    //  ItemApiController 这个控制器中 getItem 方法
    @GetMapping("api/item/{skuId}")
    Result getItem(@PathVariable Long skuId);
}
