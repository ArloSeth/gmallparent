package com.atguigu.gmall.task.test;

import com.atguigu.gmall.common.constant.MqConst;
import com.atguigu.gmall.common.service.RabbitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author mqx
 * @date 2020-11-24 15:50:38
 */
@Component
@EnableScheduling   //开启定时任务
public class ScheduledTask {

    @Autowired
    private RabbitService rabbitService;

    //  分， 时，日，月，周，年
    //  每隔30秒执行一次
    @Scheduled(cron = "0/10 * * * * ?")
    public void testTask(){
        //  直接将数据放入缓存!
        //  System.out.println("来人了，开始接客了.....");
        //  发送消息 给秒杀微服务
        rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_TASK,MqConst.ROUTING_TASK_1,"来人了，开始接客了.");
    }
}
