package com.atguigu.gmall.order.service;

import com.atguigu.gmall.model.enums.ProcessStatus;
import com.atguigu.gmall.model.order.OrderInfo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @author mqx
 * @date 2020-11-18 14:19:18
 */
public interface OrderService extends IService<OrderInfo> {

    //  保存数据
    Long saveOrderInfo(OrderInfo orderInfo);

    //  生成一个流水号
    String getTradeNo(String userId);

    /**
     * 比较流水号
     * @param userId    获取缓存
     * @param tradeNo   代表页面的流水号
     * @return
     */
    boolean checkTradeCode(String userId,String tradeNo);

    //  删除缓存的流水号
    void deleteTradeNo(String userId);

    //  查询库存
    boolean checkStock(Long skuId, Integer skuNum);

    //  关闭过期订单
    void execExpiredOrder(Long orderId);

    //  根据订单Id，进程状态更新订单
    void updateOrderStatus(Long orderId, ProcessStatus processStatus);

    /**
     * 根据订单Id 查询订单信息
     * @param orderId
     * @return
     */
    OrderInfo getOrderInfo(Long orderId);

    /**
     * 发送消息给库存
     * @param orderId
     */
    void sendOrderStatus(Long orderId);

    /**
     * 将orderInfo 部分字段转换为Map
     * @param orderInfo
     * @return
     */
    Map initWareOrder(OrderInfo orderInfo);

    /**
     * 拆单接口
     * @param orderId
     * @param wareSkuMap
     * @return
     */
    List<OrderInfo> orderSplit(long orderId, String wareSkuMap);

    /**
     * 关闭电商模块的支付记录
     * @param orderId
     * @param flag
     */
    void execExpiredOrder(Long orderId, String flag);
}
