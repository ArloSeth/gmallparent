package com.atguigu.gmall.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.common.constant.MqConst;
import com.atguigu.gmall.common.service.RabbitService;
import com.atguigu.gmall.common.util.HttpClientUtil;
import com.atguigu.gmall.model.enums.OrderStatus;
import com.atguigu.gmall.model.enums.ProcessStatus;
import com.atguigu.gmall.model.order.OrderDetail;
import com.atguigu.gmall.model.order.OrderInfo;
import com.atguigu.gmall.order.mapper.OrderDetailMapper;
import com.atguigu.gmall.order.mapper.OrderInfoMapper;
import com.atguigu.gmall.order.service.OrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.bouncycastle.util.Integers;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @author mqx
 * @date 2020-11-18 14:20:38
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderInfoMapper,OrderInfo> implements OrderService {

    @Autowired
    private OrderInfoMapper orderInfoMapper;

    @Autowired
    private OrderDetailMapper orderDetailMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RabbitService rabbitService;

    @Override
    @Transactional
    public Long saveOrderInfo(OrderInfo orderInfo) {
        //  orderDetail
        List<OrderDetail> orderDetailList = orderInfo.getOrderDetailList();
        //  orderInfo
        //  总金额，订单状态，userId, out_trade_no ,订单描述，创建时间，过期时间, 进度状态
        orderInfo.sumTotalAmount();
        //  订单状态
        orderInfo.setOrderStatus(OrderStatus.UNPAID.name());
        //  第三方交易编号
        String outTradeNo = "ATGUIGU" + System.currentTimeMillis() + "" + new Random().nextInt(1000);
        orderInfo.setOutTradeNo(outTradeNo);
        //  利用商品的名称，组成订单描述
        //        StringBuilder sb = new StringBuilder();
        //        for (OrderDetail orderDetail : orderDetailList) {
        //            sb.append(orderDetail.getSkuName());
        //        }
        //        if (sb.length()>200){
        //            orderInfo.setTradeBody(sb.toString().substring(0,200));
        //        }else {
        //            orderInfo.setTradeBody(sb.toString());
        //        }
        //  描述订单的
        orderInfo.setTradeBody("冬天冷了，买个雕! 暖和暖和!");
        //  创建时间
        orderInfo.setCreateTime(new Date());
        //  过期时间 当前系统时间+1天
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,1);
        orderInfo.setExpireTime(calendar.getTime());

        //  进度状态
        orderInfo.setProcessStatus(ProcessStatus.UNPAID.name());
        orderInfoMapper.insert(orderInfo);

        //  判断
        if (!CollectionUtils.isEmpty(orderDetailList)){
            //  循环遍历
            for (OrderDetail orderDetail : orderDetailList) {
                //  设置订单Id
                orderDetail.setOrderId(orderInfo.getId());
                orderDetailMapper.insert(orderDetail);
            }
        }
        //  返回订单Id
        return orderInfo.getId();
    }

    @Override
    public String getTradeNo(String userId) {
        //  使用UUID 创建一个流水号
        String tradeNo = UUID.randomUUID().toString();
        //  必须先获取到key
        String tradeNoKey = "user:" + userId + ":tradeCode";
        //  将流水号放入缓存
        redisTemplate.opsForValue().set(tradeNoKey,tradeNo);

        return tradeNo;
    }

    @Override
    public boolean checkTradeCode(String userId, String tradeNo) {
        //  必须先获取到key
        String tradeNoKey = "user:" + userId + ":tradeCode";
        //  获取缓存的流水号
        String tradeNoRedis = (String) redisTemplate.opsForValue().get(tradeNoKey);
        //  返回比较结果
        return tradeNo.equals(tradeNoRedis);
    }

    @Override
    public void deleteTradeNo(String userId) {
        //  必须先获取到key
        String tradeNoKey = "user:" + userId + ":tradeCode";
        //  删除
        redisTemplate.delete(tradeNoKey);
    }

    @Override
    public boolean checkStock(Long skuId, Integer skuNum) {
        //  service-order 调用ware-manage 需要远程调用，使用httpClient
        //  传入远程调用的地址 http://localhost:9001/hasStock?skuId=10221&num=2
        //  远程调用接口 返回值 0：无库存   1：有库存
        String result = HttpClientUtil.doGet("http://localhost:9001/hasStock?skuId=" + skuId + "&num=" + skuNum);
        //  返回判断结果
        return "1".equals(result);
    }

    @Override
    public void execExpiredOrder(Long orderId) {
        //  关闭订单
        //  update order_info set order_status=CLOSED , process_status = CLOSED where id = orderId;
        //        OrderInfo orderInfo = new OrderInfo();
        //        orderInfo.setId(orderId);
        //        orderInfo.setOrderStatus(ProcessStatus.CLOSED.getOrderStatus().name());
        //        //  orderInfo.setOrderStatus(OrderStatus.CLOSED.name());
        //        orderInfo.setProcessStatus(ProcessStatus.CLOSED.name());
        //        //  在进度状态ProcessStatus 中能够获取订单的状态！
        //        orderInfoMapper.updateById(orderInfo);

        //  封装一个方法将 orderId，ProcessStatus.CLOSED 作为参数,后面的业务会经常用到更新。
        updateOrderStatus(orderId, ProcessStatus.CLOSED);

        //  发送消息关闭交易记录 paymentInfo ;
        rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_PAYMENT_CLOSE,MqConst.ROUTING_PAYMENT_CLOSE,orderId);
    }

    //  更新订单！
    public void updateOrderStatus(Long orderId, ProcessStatus processStatus) {
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setId(orderId);
        orderInfo.setOrderStatus(processStatus.getOrderStatus().name());
        orderInfo.setProcessStatus(processStatus.name());
        //  在进度状态ProcessStatus 中能够获取订单的状态！
        orderInfoMapper.updateById(orderInfo);
    }

    @Override
    public OrderInfo getOrderInfo(Long orderId) {
        //  select * from order_info where id = orderId;
        OrderInfo orderInfo = orderInfoMapper.selectById(orderId);
        if (orderInfo!=null){
            //  查询订单明细
            QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
            orderDetailQueryWrapper.eq("order_id",orderId);
            List<OrderDetail> orderDetailList = orderDetailMapper.selectList(orderDetailQueryWrapper);
            //  赋值给订单主表
            orderInfo.setOrderDetailList(orderDetailList);
        }

        return orderInfo;
    }

    @Override
    public void sendOrderStatus(Long orderId) {
        //  发送消息之前：更新订单状态
        updateOrderStatus(orderId,ProcessStatus.NOTIFIED_WARE);

        //  获取发送消息内容：
        String wareJson = initWareOrder(orderId);
        //  发送消息
        rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_WARE_STOCK,MqConst.ROUTING_WARE_STOCK,wareJson);

    }

    //  通过orderId 将发送的数据封装成功Json - String
    private String initWareOrder(Long orderId) {
        //  Json 字符串组成 是由orderInfo 中的部分组成！
        //  先获取到OrderInfo
        OrderInfo orderInfo = getOrderInfo(orderId);
        //  将这个 orderInfo 中组成Json 的字符串的字段，封装到一个Map 中。
        Map map = initWareOrder(orderInfo);
        //  将map 转换为Json 字符串
        return JSON.toJSONString(map);
    }

    //  将orderInfo 部分字段转换为Map
    public Map initWareOrder(OrderInfo orderInfo) {
        //  先声明一个map 集合
        Map<String,Object> map = new HashMap();
        map.put("orderId",orderInfo.getId());
        map.put("consignee",orderInfo.getConsignee());
        map.put("consigneeTel", orderInfo.getConsigneeTel());
        map.put("orderComment", orderInfo.getOrderComment());
        map.put("orderBody", orderInfo.getTradeBody());
        map.put("deliveryAddress", orderInfo.getDeliveryAddress());
        map.put("paymentWay", "2");
        map.put("wareId", orderInfo.getWareId());// 仓库Id ，减库存拆单时需要使用！

        //  声明一个集合来存储orderDetailMap
        List<HashMap> orderDetailArrayList = new ArrayList<>();
        //  所有的数据都是来自于订单明细：OrderDetail;
        List<OrderDetail> orderDetailList = orderInfo.getOrderDetailList();
        for (OrderDetail orderDetail : orderDetailList) {
            HashMap<String, Object> orderDetailMap = new HashMap<>();
            orderDetailMap.put("skuId",orderDetail.getSkuId());
            orderDetailMap.put("skuNum",orderDetail.getSkuNum());
            orderDetailMap.put("skuName",orderDetail.getSkuName());
            //  将orderDetailMap 存储起来
            orderDetailArrayList.add(orderDetailMap);
        }

        //  [{skuId:101,skuNum:1,skuName:’小米手64G’},{skuId:201,skuNum:1,skuName:’索尼耳机’}]
        map.put("details",orderDetailArrayList);
        //  返回map集合
        return map;
    }

    @Override
    @Transactional
    public List<OrderInfo> orderSplit(long orderId, String wareSkuMap) {
        /*
        1.  获取原始订单，记录的是哪个订单要拆单
        2.  wareSkuMap 记录的是哪个仓库对应的skuId [{"wareId":"1","skuIds":["2","10"]},{"wareId":"2","skuIds":["3"]}]
            将 wareSkuMap 转换为我们能够操作的对象。
        3.  创建新的子订单，并赋值
        4.  保存新的子订单
        5.  修改原始订单状态 SPLIT
         */
        //  声明一个集合来存储子订单
        List<OrderInfo> orderInfoList = new ArrayList<>();
        OrderInfo orderInfoOrigin = getOrderInfo(orderId);
        List<Map> mapList = JSON.parseArray(wareSkuMap, Map.class);
        if (!CollectionUtils.isEmpty(mapList)){
            //  循环遍历
            for (Map map : mapList) {
                //  {"wareId":"1","skuIds":["2","10"]}
                String wareId = (String) map.get("wareId");

                //  获取对应的skuId
                List<String> skuIdList = (List<String>) map.get("skuIds");
                OrderInfo suborderInfo = new OrderInfo();
                //  属性拷贝
                BeanUtils.copyProperties(orderInfoOrigin,suborderInfo);
                //  订单Id ，总金额，父Id，仓库Id
                suborderInfo.setId(null);
                suborderInfo.setParentOrderId(orderId);
                suborderInfo.setWareId(wareId);

                //  声明一个集合来记录子订单的明细
                List<OrderDetail> orderDetails = new ArrayList<>();
                //  总金额需要根据订单明细来计算！
                //  需要知道当前子订单的明细是谁? 子订单明细来自于 原始订单明细。
                List<OrderDetail> orderDetailList = orderInfoOrigin.getOrderDetailList();
                //  判断原始订单明细与仓库Id 对应的skuId 是否相同，相同则是订单明细。
                for (OrderDetail orderDetail : orderDetailList) {
                    //  循环仓库id 对应的skuId
                    for (String skuId : skuIdList) {
                        //  比较相等，则将数据进行记录。
                        if (orderDetail.getSkuId().toString().equals(skuId)){
                            orderDetails.add(orderDetail);
                        }
                    }
                }
                //  设置子订单明细
                suborderInfo.setOrderDetailList(orderDetails);
                suborderInfo.sumTotalAmount();

                //  保存子订单
                saveOrderInfo(suborderInfo);

                //  将子订单添加到集合
                orderInfoList.add(suborderInfo);
            }
        }
        //  修改订单状态
        updateOrderStatus(orderId,ProcessStatus.SPLIT);

        return orderInfoList;
    }

    @Override
    public void execExpiredOrder(Long orderId, String flag) {
        //  封装一个方法将 orderId，ProcessStatus.CLOSED 作为参数,后面的业务会经常用到更新。
        updateOrderStatus(orderId, ProcessStatus.CLOSED);
        //  如果有交易记录则关闭交易记录。
        if ("2".equals(flag)){
            //  发送消息关闭交易记录 paymentInfo ;
            rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_PAYMENT_CLOSE,MqConst.ROUTING_PAYMENT_CLOSE,orderId);
        }


    }

}


