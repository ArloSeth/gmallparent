package com.atguigu.gmall.order.config;

import com.atguigu.gmall.common.constant.MqConst;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * @author mqx
 * @date 2020-11-21 11:16:33
 */
@Configuration
public class OrderCanelMqConfig {

    //  设置一个交换机
    @Bean
    public CustomExchange delayExchange(){
        HashMap<String, Object> map = new HashMap<>();
        //  设置交换机参数
        map.put("x-delayed-type","direct");
        //  直接new
        return new CustomExchange(MqConst.EXCHANGE_DIRECT_ORDER_CANCEL,"x-delayed-message",true,false,map);

    }
    //  设置一个队列
    @Bean
    public Queue delayQueue(){
        return new Queue(MqConst.QUEUE_ORDER_CANCEL,true,false,false,null);
    }

    //  设置绑定关系
    @Bean
    public Binding delayBinding1(){
        //  返回设置
        return BindingBuilder.bind(delayQueue()).to(delayExchange()).with(MqConst.ROUTING_ORDER_CANCEL).noargs();
    }
}
