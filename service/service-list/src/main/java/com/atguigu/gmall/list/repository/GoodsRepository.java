package com.atguigu.gmall.list.repository;

import com.atguigu.gmall.model.list.Goods;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author mqx
 * @date 2020-11-12 14:24:26
 */
public interface GoodsRepository extends ElasticsearchRepository<Goods,Long> {
}
