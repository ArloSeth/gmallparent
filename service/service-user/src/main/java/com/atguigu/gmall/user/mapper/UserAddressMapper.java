package com.atguigu.gmall.user.mapper;

import com.atguigu.gmall.model.user.UserAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author mqx
 * @date 2020-11-18 10:24:04
 */
@Mapper
public interface UserAddressMapper extends BaseMapper<UserAddress> {
}
