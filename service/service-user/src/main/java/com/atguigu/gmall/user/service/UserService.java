package com.atguigu.gmall.user.service;

import com.atguigu.gmall.model.user.UserInfo;

/**
 * @author mqx
 * @date 2020-11-14 15:15:57
 */
public interface UserService {

    //  登录：
    //  select * from user_info where username = ? and password = ?
    //    boolean login(String username, String password);
    //
    //    boolean login(UserInfo userInfo);
    //
    //    UserInfo login(String username, String password);

    UserInfo login(UserInfo userInfo);
    //  方法的返回值类型不能作为方法重载的依据！


}
