package com.atguigu.gmall.user.service.impl;

import com.atguigu.gmall.model.user.UserInfo;
import com.atguigu.gmall.user.mapper.UserInfoMapper;
import com.atguigu.gmall.user.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

/**
 * @author mqx
 * @date 2020-11-14 15:18:28
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public UserInfo login(UserInfo userInfo) {
        //  select * from user_info where username = ? and password = ?
        //  密码是加密操作：
        String passwd = userInfo.getPasswd(); // 111111
        //  在此将111111 加密 96e79218965eb72c92a549dd5a330112
        String newPasswd = DigestUtils.md5DigestAsHex(passwd.getBytes());

        QueryWrapper<UserInfo> userInfoQueryWrapper = new QueryWrapper<>();
        userInfoQueryWrapper.eq("login_name",userInfo.getLoginName());
        userInfoQueryWrapper.eq("passwd",newPasswd);

        UserInfo info = userInfoMapper.selectOne(userInfoQueryWrapper);
        if (info!=null){
            return info;
        }
        return null;
    }
}
