package com.atguigu.gmall.user.controller;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.common.constant.RedisConst;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.util.IpUtil;
import com.atguigu.gmall.model.user.UserInfo;
import com.atguigu.gmall.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author mqx
 * http://passport.atguigu.cn/login.html?originUrl=http://item.atguigu.cn/
 * 每个页面的排头  window.location.href = 'http://passport.gmall.com/login.html?originUrl='+window.location.href
 *
 * login.html 应该web-all.
 * @date 2020-11-14 15:25:37
 */
@RestController
@RequestMapping("/api/user/passport")
public class PassportApiController {

    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate redisTemplate;

    //  因为页面传递的json 字符串。@RequestBody 接收数据
    @PostMapping("login")
    public Result login(@RequestBody UserInfo userInfo, HttpServletRequest request){
        //  调用服务层登录方法
        UserInfo info = userService.login(userInfo);
        //  业务判断是否登录成功
        if (info!=null){
            //  登录成功 1. 需要存储一个token，需要将这个 token 返回个前端页面  2.  需要将用户信息存储 到 redis
            Map<String, Object> hashMap = new HashMap<>();
            String token = UUID.randomUUID().toString();
            //  将token放入map 中！
            hashMap.put("token",token);
            //  目的是显示用户的昵称！
            String nickName = info.getNickName();
            hashMap.put("nickName",nickName);

            //  将用户信息存储到缓存！ redisTemplate 分析使用的数据类型，以及key！
            //  用户信息{userId} key = 这个key 不能重复！ String
            //  RedisConst.USER_LOGIN_KEY_PREFIX + token  user:login:token
            //  定义存储用户信息的key
            String userKey = RedisConst.USER_LOGIN_KEY_PREFIX + token;
            //  为了防止token 被盗用，再需要存储一个ip 地址！ ，如果token 被盗用！ 验证ip 是否一致！
            //  现在要存储的数据， userId,ip 地址！
            JSONObject userJson = new JSONObject();
            userJson.put("userId",info.getId().toString());
            userJson.put("ip", IpUtil.getIpAddress(request));

            redisTemplate.opsForValue().set(userKey,userJson.toJSONString(),RedisConst.USERKEY_TIMEOUT, TimeUnit.SECONDS);
            // xxx 要么是个对象，要么是个map
            return Result.ok(hashMap);
        }else{
            //  登录失败！
            return Result.fail().message("登录失败！");
        }

    }


    //  用户退出控制器
    //  如果登录了，那么cookie 中有 token ，header 中也有token！
    //  退出登录，cookie中token js 会自动删除，将缓存中的userInfo 信息删除即可！
    @GetMapping("logout")
    public Result logout(HttpServletRequest request){
        //  将缓存中的userInfo 信息删除即可！
        String token = request.getHeader("token");
        //  key = RedisConst.USER_LOGIN_KEY_PREFIX + token  user:login:token
        String userKey = RedisConst.USER_LOGIN_KEY_PREFIX + token;
        redisTemplate.delete(userKey);
        return Result.ok();
    }

}
