package com.atguigu.gmall.user.service;

import com.atguigu.gmall.model.user.UserAddress;

import java.util.List;

/**
 * @author mqx
 * @date 2020-11-18 10:24:45
 */
//  用户地址列表接口
public interface UserAddressService {

    //  根据用户Id 查询
    List<UserAddress> findUserAddressListByUserId(String userId);
}
