package com.atguigu.gmall.payment.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.model.enums.PaymentType;
import com.atguigu.gmall.model.payment.PaymentInfo;
import com.atguigu.gmall.payment.config.AlipayConfig;
import com.atguigu.gmall.payment.service.AlipayService;
import com.atguigu.gmall.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * @author mqx
 * 这个类中，有些控制器是返回的Json ，但是有些不是
 * @date 2020-11-23 09:20:54
 */
@Controller
@RequestMapping("/api/payment/alipay")
public class AlipayController {

    @Autowired
    private AlipayService alipayService;

    @Autowired
    private PaymentService paymentService;

    //  http://api.gmall.com/api/payment/alipay/submit/55
    @RequestMapping("submit/{orderId}")
    @ResponseBody // 需要将实现类中返回的from 表单数据直接渲染到页面！
    public String submitOrder(@PathVariable Long orderId){
        String from = null;
        try {
            from = alipayService.createaliPay(orderId);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return from;
    }

    //  同步回调
    //  http://api.gmall.com/api/payment/alipay/callback/return
    @RequestMapping("callback/return")
    public String callBack() {
        //  同步回调给用户展示信息
        //  return_order_url=http://payment.gmall.com/pay/success.html
        return "redirect:" + AlipayConfig.return_order_url;
    }

    //  异步回调：callback/notify 验证成功：success
    //  https: //商家网站通知地址?voucher_detail_list=[{"amount":"0.20","merchantContribute":"0.00","name":"5折券","otherContribute":"0.20","type":"ALIPAY_DISCOUNT_VOUCHER","voucherId":"2016101200073002586200003BQ4"}]&fund_bill_list=[{"amount":"0.80","fundChannel":"ALIPAYACCOUNT"},{"amount":"0.20","fundChannel":"MDISCOUNT"}]&subject=PC网站支付交易&trade_no=2016101221001004580200203978&gmt_create=2016-10-12 21:36:12&notify_type=trade_status_sync&total_amount=1.00&out_trade_no=mobile_rdm862016-10-12213600&invoice_amount=0.80&seller_id=2088201909970555&notify_time=2016-10-12 21:41:23&trade_status=TRADE_SUCCESS&gmt_payment=2016-10-12 21:37:19&receipt_amount=0.80&passback_params=passback_params123&buyer_id=2088102114562585&app_id=2016092101248425&notify_id=7676a2e1e4e737cff30015c4b7b55e3kh6& sign_type=RSA2&buyer_pay_amount=0.80&sign=***&point_amount=0.00
    @RequestMapping("callback/notify")
    @ResponseBody
    public String alipayNotify(@RequestParam Map<String, String> paramMap){
        //  调用验签方法
        boolean  signVerified = false;  //调用SDK验证签名
        try {
            signVerified = AlipaySignature.rsaCheckV1(paramMap, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        //  获取到返回的交易状态：
        String tradeStatus = paramMap.get("trade_status");
        //  获取out_trade_no 支付宝异步回调过来的！
        String outTradeNo = paramMap.get("out_trade_no");
        //  扩展一个业务，有支付宝，微信支付，
        PaymentInfo paymentInfo = paymentService.getPaymentInfo(outTradeNo, PaymentType.ALIPAY.name());
        //  看是否能够获取到paymentInfo ！
        //        if (paymentInfo!=null){
        //            //  验证out_trade_no，
        //        }
        //        String totalAmount = paramMap.get("total_amount");
        //        totalAmount == paymentInfo.getTotalAmount();

        if (signVerified){
            //  TODO 验签成功后，按照支付结果异步通知中的描述，对支付结果中的业务内容进行二次校验，校验成功后在response中返回success并继续商户自身业务处理，校验失败返回failure
            //  校验用户是否付款成功 只有交易通知状态为 TRADE_SUCCESS 或 TRADE_FINISHED 时，支付宝才会认定为买家付款成功
            if ("TRADE_SUCCESS".equals(tradeStatus)||"TRADE_FINISHED".equals(tradeStatus)){
                //  细节问题：在异步回调过程中！如果支付状态为PAID或者是已经CLOSED
                //  需要查询paymentInfo
                if ("PAID".equals(paymentInfo.getPaymentStatus()) || "ClOSED".equals(paymentInfo.getPaymentStatus())){
                    return "failure";
                }
                //  表示支付成功！
                //  需要改变支付交易记录的状态！paymentInfo PaymentStatus = PAID
                //  调用一个方法
                //  out_trade_no，payment_type 更新条件： trade_no,payment_status,callback_time,callback_content 更新内容
                paymentService.paySuccess(outTradeNo,PaymentType.ALIPAY.name(), paramMap);
                return "success";
            }

        } else {
            // TODO 验签失败则记录异常日志，并在response中返回failure.
            return "failure";
        }
        return "failure";
    }

    //  发起退款请求：
    @RequestMapping("refund/{orderId}")
    @ResponseBody
    public Result refund(@PathVariable Long orderId) {
        //  调用退款数据接口
        boolean flag = alipayService.refund(orderId);
        return Result.ok(flag);
    }

    //  关闭支付宝交易记录
    //  http://localhost:8205/api/payment/alipay/closePay/25
    @GetMapping("closePay/{orderId}")
    @ResponseBody
    public Boolean closePay(@PathVariable Long orderId){
        Boolean aBoolean = alipayService.closePay(orderId);
        return aBoolean;
    }

    //  查询是否有交易记录
    //  http://localhost:8205/api/payment/alipay/checkPayment/30
    @RequestMapping("checkPayment/{orderId}")
    @ResponseBody
    public Boolean checkPayment(@PathVariable Long orderId){
        // 调用退款接口
        boolean flag = alipayService.checkPayment(orderId);
        return flag;
    }

    //  查询是否有交易记录的数据接口：
    @GetMapping("getPaymentInfo/{outTradeNo}")
    @ResponseBody
    public PaymentInfo getPaymentInfo(@PathVariable String outTradeNo){
        PaymentInfo paymentInfo = paymentService.getPaymentInfo(outTradeNo, PaymentType.ALIPAY.name());
        if (null!=paymentInfo){
            return paymentInfo;
        }
        return null;
    }

}
