package com.atguigu.gmall.payment.service.impl;

import com.atguigu.gmall.common.constant.MqConst;
import com.atguigu.gmall.common.service.RabbitService;
import com.atguigu.gmall.model.enums.PaymentStatus;
import com.atguigu.gmall.model.order.OrderInfo;
import com.atguigu.gmall.model.payment.PaymentInfo;
import com.atguigu.gmall.payment.mapper.PaymentInfoMapper;
import com.atguigu.gmall.payment.service.PaymentService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * @author mqx
 * @date 2020-11-21 15:38:42
 */
@Service
public class PaymentServiceImpl implements PaymentService {
    //  必须引入mapper
    @Autowired
    private PaymentInfoMapper paymentInfoMapper;

    @Autowired
    private RabbitService rabbitService;

    @Override
    public void savePaymentInfo(OrderInfo orderInfo, String paymentType) {
        //  paymentInfo 中的数据，部分信息是来自于 orderInfo.
        //  select count(*) from payment_info where order_id = orderId and payment_type = paymentType ;
        QueryWrapper<PaymentInfo> paymentInfoQueryWrapper = new QueryWrapper<>();
        paymentInfoQueryWrapper.eq("order_id",orderInfo.getId()).eq("payment_type",paymentType);
        //  查询是否有记录
        Integer count = paymentInfoMapper.selectCount(paymentInfoQueryWrapper);
        if (count>0) return;
        //  创建一个paymentInfo
        PaymentInfo paymentInfo = new PaymentInfo();
        paymentInfo.setOutTradeNo(orderInfo.getOutTradeNo());
        paymentInfo.setOrderId(orderInfo.getId());
        paymentInfo.setPaymentType(paymentType);
        //  支付的交易编号，保存的时候，是获取不到的，所以暂时不给
        //  paymentInfo.setTradeNo();
        paymentInfo.setTotalAmount(orderInfo.getTotalAmount());
        paymentInfo.setSubject(orderInfo.getTradeBody());
        paymentInfo.setPaymentStatus(PaymentStatus.UNPAID.name());
        paymentInfo.setCreateTime(new Date());
        //  回调是指支付宝支付完成之后的异步回调时间，所以暂时不给！
        paymentInfoMapper.insert(paymentInfo);

    }

    @Override
    public PaymentInfo getPaymentInfo(String outTradeNo, String name) {
        //  select * from payment_info where out_trade_no = outTradeNo and payment_type = name
        QueryWrapper<PaymentInfo> paymentInfoQueryWrapper = new QueryWrapper<>();
        paymentInfoQueryWrapper.eq("out_trade_no",outTradeNo);
        paymentInfoQueryWrapper.eq("payment_type",name);
        return paymentInfoMapper.selectOne(paymentInfoQueryWrapper);
    }

    @Override
    public void paySuccess(String outTradeNo, String name, Map<String, String> paramMap) {
        //  out_trade_no，payment_type 更新条件： trade_no,payment_status,callback_time,callback_content 更新内容
        //  更新的内容
        PaymentInfo paymentInfo = new PaymentInfo();
        paymentInfo.setPaymentStatus(PaymentStatus.PAID.name());
        paymentInfo.setCallbackTime(new Date());
        paymentInfo.setTradeNo(paramMap.get("trade_no"));
        paymentInfo.setCallbackContent(paramMap.toString());

        //  设置更新的条件
        //        QueryWrapper<PaymentInfo> paymentInfoQueryWrapper = new QueryWrapper<>();
        //        paymentInfoQueryWrapper.eq("out_trade_no",outTradeNo);
        //        paymentInfoQueryWrapper.eq("payment_type",name);
        //        paymentInfoMapper.update(paymentInfo,paymentInfoQueryWrapper);

        this.updatePaymentInfo(outTradeNo,name,paymentInfo);

        //  paymentInfo 这个对象是更新的，没有订单Id
        PaymentInfo paymentInfoQuery = getPaymentInfo(outTradeNo, name);
        //  发送消息 发送订单Id
        this.rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_PAYMENT_PAY,MqConst.ROUTING_PAYMENT_PAY,paymentInfoQuery.getOrderId());
    }

    public void updatePaymentInfo(String outTradeNo, String name, PaymentInfo paymentInfo) {
        QueryWrapper<PaymentInfo> paymentInfoQueryWrapper = new QueryWrapper<>();
        paymentInfoQueryWrapper.eq("out_trade_no",outTradeNo);
        paymentInfoQueryWrapper.eq("payment_type",name);
        paymentInfoMapper.update(paymentInfo,paymentInfoQueryWrapper);
    }

    @Override
    public void closePayment(Long orderId) {
        // update payment_info set payment_status = ClOSED where order_id = orderId;
        //  第一个参数：paymentInfo 表示什么修改的内容是啥。第二个参数：更新条件
        PaymentInfo paymentInfo = new PaymentInfo();
        paymentInfo.setPaymentStatus(PaymentStatus.ClOSED.name());

        QueryWrapper<PaymentInfo> paymentInfoQueryWrapper = new QueryWrapper<>();
        paymentInfoQueryWrapper.eq("order_id",orderId);

        //  有记录关闭，没有记录不关闭！  paymentInfo 什么时候才会产生数据?
        Integer count = paymentInfoMapper.selectCount(paymentInfoQueryWrapper);
        if (count==0){
            return;
        }
        paymentInfoMapper.update(paymentInfo,paymentInfoQueryWrapper);

    }
}
