package com.atguigu.gmall.payment.mapper;

import com.atguigu.gmall.model.payment.PaymentInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author mqx
 * @date 2020-11-21 15:39:09
 */
@Mapper
public interface PaymentInfoMapper extends BaseMapper<PaymentInfo> {
}
