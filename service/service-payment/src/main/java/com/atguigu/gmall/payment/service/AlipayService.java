package com.atguigu.gmall.payment.service;

import com.alipay.api.AlipayApiException;

/**
 * @author mqx
 * @date 2020-11-23 09:10:14
 */
//  处理支付支付
public interface AlipayService {
    //  根据订单Id 进行支付
    String createaliPay(Long orderId) throws AlipayApiException;

    //  退款
    boolean refund(Long orderId);

    /***
     * 关闭交易
     * @param orderId
     * @return
     */
    Boolean closePay(Long orderId);

    /**
     * 根据订单查询是否支付成功！
     * @param orderId
     * @return
     */
    Boolean checkPayment(Long orderId);
}
