package com.atguigu.gmall.activity.controller;

import com.atguigu.gmall.activity.service.SeckillGoodsService;
import com.atguigu.gmall.activity.util.CacheHelper;
import com.atguigu.gmall.common.constant.MqConst;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.result.ResultCodeEnum;
import com.atguigu.gmall.common.service.RabbitService;
import com.atguigu.gmall.common.util.AuthContextHolder;
import com.atguigu.gmall.common.util.DateUtil;
import com.atguigu.gmall.common.util.MD5;
import com.atguigu.gmall.model.activity.SeckillGoods;
import com.atguigu.gmall.model.activity.UserRecode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author mqx
 * @date 2020-11-25 10:56:48
 */
@RestController
@RequestMapping("/api/activity/seckill")
public class SeckillGoodsApiController {

    @Autowired
    private SeckillGoodsService seckillGoodsService;

    @Autowired
    private RabbitService rabbitService;

    @GetMapping("/findAll")
    public Result findAll() {
        return Result.ok(seckillGoodsService.findAll());
    }

    /**
     * 获取实体
     *
     * @param skuId
     * @return
     */
    @GetMapping("/getSeckillGoods/{skuId}")
    public Result getSeckillGoods(@PathVariable("skuId") Long skuId) {
        return Result.ok(seckillGoodsService.getSeckillGoods(skuId));
    }

    //  获取下单码：
    @GetMapping("auth/getSeckillSkuIdStr/{skuId}")
    public Result getSeckillSkuIdStr(@PathVariable Long skuId, HttpServletRequest request){
        //  我们定义为当前用户id MD5加密！
        String userId = AuthContextHolder.getUserId(request);

        //  查看一下skuId 对应的秒杀商品是否存在！
        SeckillGoods seckillGoods = seckillGoodsService.getSeckillGoods(skuId);
        if (seckillGoods!=null){
            //  只有在商品秒杀时间范围内，才能获取下单码 在秒杀活动开始之后，结束之前, 跟当前系统时间
            Date curTime = new Date();
            //  使用工具类
            if (DateUtil.dateCompare(seckillGoods.getStartTime(),curTime)
                    && DateUtil.dateCompare(curTime,seckillGoods.getEndTime())){
                    //  用户Id！
                    String encrypt = MD5.encrypt(userId);
                    //  返回下单码
                    return Result.ok(encrypt);
            }
        }

        //  返回值，获取下单码失败！
        return Result.fail().message("获取下单码失败!");
    }

    //  url: /api/activity/seckill/auth/seckillOrder/skuId?skuIdStr=skuIdStr
    //  保存订单：
    @PostMapping("auth/seckillOrder/{skuId}")
    public Result seckillOrder(@PathVariable Long skuId,HttpServletRequest request){
        //  获取传递过来的下单码！
        String skuIdStr = request.getParameter("skuIdStr");
        //  下单码的生成规则是将userId 进行md5加密。
        String userId = AuthContextHolder.getUserId(request);
        //  对其进行加密
        String encrypt = MD5.encrypt(userId);
        //  校验下单码：
        if (!skuIdStr.equals(encrypt)){
            /*
               1.   如果前台只是要200的话，那就是OK。如果不只要200 ，需要借助build();    在做单点登录与业务整合时。
               2.   页面需要返回message  Result.fail().message("提示信息的！");

             */
            return Result.build(null, ResultCodeEnum.SECKILL_ILLEGAL);
        }

        //  校验状态位：  存储数据：key = skuId value = 状态位
        String state  = (String) CacheHelper.get(skuId.toString());
        //  进行判断    null
        if (StringUtils.isEmpty(state)){
            //  返回提示信息
            return Result.build(null, ResultCodeEnum.SECKILL_ILLEGAL);
        }else if("1".equals(state)){
            //  有一个对象用来记录哪个用户秒杀的哪个商品
            UserRecode userRecode = new UserRecode();
            userRecode.setUserId(userId);
            userRecode.setSkuId(skuId);

            //  发送到消息队列中
            //  MqConst.EXCHANGE_DIRECT_SECKILL_USER
            rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_SECKILL_USER,MqConst.ROUTING_SECKILL_USER,userRecode);
            return Result.ok();
        }else {
            //  如果是0 ，则表示商品已经售罄
            //  返回提示信息
            return Result.build(null, ResultCodeEnum.SECKILL_FINISH);
        }

    }

}
