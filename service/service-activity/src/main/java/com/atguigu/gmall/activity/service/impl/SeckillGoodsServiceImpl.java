package com.atguigu.gmall.activity.service.impl;

import com.atguigu.gmall.activity.mapper.SeckillGoodsMapper;
import com.atguigu.gmall.activity.service.SeckillGoodsService;
import com.atguigu.gmall.activity.util.CacheHelper;
import com.atguigu.gmall.common.constant.RedisConst;
import com.atguigu.gmall.common.util.MD5;
import com.atguigu.gmall.model.activity.OrderRecode;
import com.atguigu.gmall.model.activity.SeckillGoods;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author mqx
 * @date 2020-11-25 10:48:27
 */
@Service
public class SeckillGoodsServiceImpl implements SeckillGoodsService {

    //  注入mapper！
    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private SeckillGoodsMapper seckillGoodsMapper;

    @Override
    public List<SeckillGoods> findAll() {
        //  获取缓存中所有秒杀商品的数据 hash ,hset (key,field,value) hget(key,field)
        List<SeckillGoods> seckillGoodsList = redisTemplate.boundHashOps(RedisConst.SECKILL_GOODS).values();
        return seckillGoodsList;
    }

    @Override
    public SeckillGoods getSeckillGoods(Long skuId) {
        //  hget(key,field)
        SeckillGoods seckillGoods = (SeckillGoods) redisTemplate.boundHashOps(RedisConst.SECKILL_GOODS).get(skuId.toString());
        return seckillGoods;
    }

    @Override
    public void seckillOrder(String userId, Long skuId) {
        //  校验状态位：  存储数据：key = skuId value = 状态位
        String state  = (String) CacheHelper.get(skuId.toString());
        //  1 : 有商品，可以下单，0:没有商品不能下单
        if ("0".equals(state)){
            return;
        }
        //  判断用户是否已经下过订单 使用setnx() 如果用户下过订单了，那么会将数据存储到缓存！
        //  缓存存储的数据类型，以及key！RedisConst.SECKILL_USER + userId  || seckill:user:userId  value = skuId
        //  set(seckill:user:userId ,skuId)
        Boolean flag = redisTemplate.opsForValue().setIfAbsent(RedisConst.SECKILL_USER + userId, skuId.toString(), RedisConst.SECKILL__TIMEOUT, TimeUnit.SECONDS);
        //  判断flag=true 表示第一次购买，如果是false 则用户已经下过订单
        if (!flag){
            return;
        }
        //  对应减库存   seckill:stock:31
        String skuIdIsExist = (String) redisTemplate.boundListOps(RedisConst.SECKILL_STOCK_PREFIX + skuId.toString()).rightPop();
        //  如果skuIdIsExist 获取到的不是空，则说明秒杀成功！反之
        if (StringUtils.isEmpty(skuIdIsExist)){
            //  如果商品的状态位是0，则需要通知其他兄弟节点更新状态位！
            redisTemplate.convertAndSend("seckillpush",skuId+":0");
            return;
        }
        //  如果能够下单：将数据保存到 OrderRecode 秒杀订单记录
        OrderRecode orderRecode = new OrderRecode();
        //  一顿赋值
        orderRecode.setUserId(userId);
        //  表示下单码
        orderRecode.setOrderStr(MD5.encrypt(userId+skuId));
        orderRecode.setNum(1);
        orderRecode.setSeckillGoods(this.getSeckillGoods(skuId));

        //  将数据放入缓存！    数据类型Hash + key  RedisConst.SECKILL_ORDERS
        //  key = seckill:orders field = userId value = orderRecode
        redisTemplate.boundHashOps(RedisConst.SECKILL_ORDERS).put(userId,orderRecode);

        //  更新库存！   缓存 + mysql
        this.updateStockCount(skuId);
    }

    //  更新库存！
    private void updateStockCount(Long skuId) {
        //  获取剩余库存的数量  List = seckill:stock:31
        Long count = redisTemplate.boundListOps(RedisConst.SECKILL_STOCK_PREFIX + skuId.toString()).size();
        //  为了避免频繁更新数据库，可以做个操作！
        if (count%2==0){
            //  更新数据库
            //            SeckillGoods seckillGoods = new SeckillGoods();
            //            seckillGoods.setStockCount(count.intValue());
            //            QueryWrapper<SeckillGoods> seckillGoodsQueryWrapper = new QueryWrapper<>();
            //            seckillGoodsQueryWrapper.eq("sku_id",skuId);
            //            seckillGoodsMapper.update(seckillGoods,seckillGoodsQueryWrapper);

            //  获取缓存的对象 是一个有 Id的SeckillGoods
            SeckillGoods seckillGoodsRedis = this.getSeckillGoods(skuId);
            seckillGoodsRedis.setStockCount(count.intValue());
            //  更新数据库
            seckillGoodsMapper.updateById(seckillGoodsRedis);
            //  更新缓存！
            //  RedisConst.SECKILL_GOODS; seckill:goods
            redisTemplate.boundHashOps(RedisConst.SECKILL_GOODS).put(skuId.toString(),seckillGoodsRedis);
        }

    }
}
