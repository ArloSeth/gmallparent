package com.atguigu.gmall.activity.service;

import com.atguigu.gmall.model.activity.SeckillGoods;

import java.util.List;

/**
 * @author mqx
 * @date 2020-11-25 10:46:57
 */
public interface SeckillGoodsService {

    //  查询所有的秒杀商品
    List<SeckillGoods> findAll();
    //  根据skuId 获取到商品的详情页面
    SeckillGoods getSeckillGoods(Long skuId);

    //  预下单
    void seckillOrder(String userId, Long skuId);
}
