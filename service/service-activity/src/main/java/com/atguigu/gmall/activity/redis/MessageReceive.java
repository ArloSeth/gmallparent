package com.atguigu.gmall.activity.redis;

import com.atguigu.gmall.activity.util.CacheHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class MessageReceive {

    /**接收消息的方法*/
    public void receiveMessage(String message){
        System.out.println("----------收到消息了message："+message);
        if(!StringUtils.isEmpty(message)) {
            /*
             消息格式
                skuId:0 表示没有商品
                skuId:1 表示有商品
             */
            //  发送过来的数据 ""6:1"" 将" 替换 空
            message = message.replaceAll("\"","");
            String[] split = StringUtils.split(message, ":");
//            String[] split = message.split(":");

            if (split == null || split.length == 2) {
                //  存储数据：key = skuId value = 状态位
                //  本质是Map ，服务器启动的时候，数据是存在的，如果服务器停止了，在启动。则原来的数据会消失！
                CacheHelper.put(split[0], split[1]);
            }
        }
    }

}
