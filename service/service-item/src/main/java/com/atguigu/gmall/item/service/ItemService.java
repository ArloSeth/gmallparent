package com.atguigu.gmall.item.service;

import java.util.Map;

/**
 * @author mqx
 * @date 2020-11-7 11:47:12
 */
public interface ItemService {

    //  数据接口重点做汇总！
    Map<String ,Object> getBySkuId(Long skuId);

}
