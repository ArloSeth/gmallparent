package com.atguigu.gmall.item.service.imp;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.item.service.ItemService;
import com.atguigu.gmall.list.client.ListFeignClient;
import com.atguigu.gmall.model.product.BaseCategoryView;
import com.atguigu.gmall.model.product.SkuInfo;
import com.atguigu.gmall.model.product.SpuInfo;
import com.atguigu.gmall.model.product.SpuSaleAttr;
import com.atguigu.gmall.product.client.ProductFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @author mqx
 * @date 2020-11-7 11:51:57
 */
@Service
public class ItemServiceImpl implements ItemService {

    //  远程调用prodcut
    @Autowired
    private ProductFeignClient productFeignClient;

    @Autowired
    private ThreadPoolExecutor threadPoolExecutor;

    @Autowired
    private ListFeignClient listFeignClient;

    @Override
    public Map<String, Object> getBySkuId(Long skuId) {

        Map<String, Object> map = new HashMap<>();

        //  获取数据skuInfo + skuImage
        CompletableFuture<SkuInfo> skuInfoCompletableFuture = CompletableFuture.supplyAsync(() -> {
            SkuInfo skuInfo = productFeignClient.getSkuInfo(skuId);
            //  存储到map中
            map.put("skuInfo",skuInfo);
            return skuInfo;
        }, threadPoolExecutor);

        //  获取价格
        CompletableFuture<Void> skuPriceCompletableFuture = CompletableFuture.runAsync(() -> {
            BigDecimal skuPrice = productFeignClient.getSkuPrice(skuId);
            //  存储到map中
            map.put("price", skuPrice);
        }, threadPoolExecutor);

        //  获取分类
        //  Consumer
        CompletableFuture<Void> categoryViewCompletableFuture = skuInfoCompletableFuture.thenAcceptAsync((skuInfo) -> {
            BaseCategoryView categoryView = productFeignClient.getCategoryView(skuInfo.getCategory3Id());
            //  存储到map中
            map.put("categoryView", categoryView);
        }, threadPoolExecutor);


        //  获取销售属性数据
        CompletableFuture<Void> spuCompletableFuture = skuInfoCompletableFuture.thenAcceptAsync((skuInfo) -> {
            List<SpuSaleAttr> spuSaleAttrListCheckBySku = productFeignClient.getSpuSaleAttrListCheckBySku(skuId, skuInfo.getSpuId());
            //  存储到map中
            map.put("spuSaleAttrList",spuSaleAttrListCheckBySku);
        }, threadPoolExecutor);

        //  获取销售属性值Id 与 skuId 组成的map
        CompletableFuture<Void> mapCompletableFuture = skuInfoCompletableFuture.thenAcceptAsync((skuInfo) -> {
            Map skuValueIdsMap = productFeignClient.getSkuValueIdsMap(skuInfo.getSpuId());
            String valuesSkuJson = JSON.toJSONString(skuValueIdsMap);
            //  存储到map中
            map.put("valuesSkuJson", valuesSkuJson);
        }, threadPoolExecutor);

        //  记录商品被访问的次数
        CompletableFuture<Void> hotScoreCompletableFuture = CompletableFuture.runAsync(() -> {
            //  远程调用热度排名方法
            listFeignClient.incrHotScore(skuId);
        }, threadPoolExecutor);

        //  使用多任务组合 allOf
        CompletableFuture.allOf(
                skuInfoCompletableFuture,
                skuPriceCompletableFuture,
                categoryViewCompletableFuture,
                spuCompletableFuture,
                mapCompletableFuture,
                hotScoreCompletableFuture
                ).join();

        return map;
    }
}
