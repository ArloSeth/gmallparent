package com.atguigu.gmall.mq.receiver;

import com.atguigu.gmall.mq.config.DeadLetterMqConfig;
import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author mqx
 * @date 2020-11-21 09:44:09
 */
@Component
public class DeadLetterReceiver {

    //  消息监听者：
    //  bindings -  配置绑定关系！
    //  直接监听队列
    @SneakyThrows
    @RabbitListener(queues = DeadLetterMqConfig.queue_dead_2)
    public void getMsg(String msg, Message message, Channel channel){
        //  获取到当前时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(simpleDateFormat.format(new Date())+"接收的消息：\t"+msg);

        //  确认消息
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }
}
