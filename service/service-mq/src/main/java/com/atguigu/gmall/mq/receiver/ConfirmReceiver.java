package com.atguigu.gmall.mq.receiver;

import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author mqx
 * 接收消息
 * @date 2020-11-20 14:31:08
 */
@Component
public class ConfirmReceiver {

    //  使用注解来接收
    //  配置交换机与队列的绑定关系！
    @SneakyThrows
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "queue.confirm",durable = "true",autoDelete = "false"),
            exchange = @Exchange(value = "exchange.confirm"),
            key = {"routing.confirm"}
    ))
    public void getMsg(String haha, Message message, Channel channel){

        System.out.println(haha);
        byte[] body = message.getBody();
        //  在此将字节数组转换为String
        System.out.println("接收到的消息：\t"+new String(body));

        //  消息确认    ,   是否批量确认数据！
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }
}
