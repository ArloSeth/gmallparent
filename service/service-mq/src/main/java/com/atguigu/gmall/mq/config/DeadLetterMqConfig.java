package com.atguigu.gmall.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * @author mqx
 * @date 2020-11-21 09:19:51
 */
@Configuration
public class DeadLetterMqConfig {
    //  定义变量
    public static final String exchange_dead = "exchange.dead";
    public static final String routing_dead_1 = "routing.dead.1";
    public static final String routing_dead_2 = "routing.dead.2";
    public static final String queue_dead_1 = "queue.dead.1";
    public static final String queue_dead_2 = "queue.dead.2";

    //  将交换机，队列，绑定关系放入spring 容器。
    //  制作一个交换机
    @Bean
    public DirectExchange exchange(){

        //  创建交换机 交换机名称，是否持久化，是否自动删除
        return new DirectExchange(exchange_dead,true,false);
    }
    //  制作一个队列
    @Bean
    public Queue queue1(){
        HashMap<String, Object> map = new HashMap<>();
        //  设置消息的过期时间 10秒钟
        map.put("x-message-ttl",10000);
        //  设置死信交换机
        map.put("x-dead-letter-exchange",exchange_dead);
        //
        map.put("x-dead-letter-routing-key",routing_dead_2);
        //  第三个参数：是否排外 ，第五个参数：当前队列中要设置其他参数：
        return new Queue(queue_dead_1,true,false,false,map);
    }
    //  制作绑定关系
    @Bean
    public Binding binding(){
        //  直接 new 同时也可以采用 BindingBuilder
        return BindingBuilder.bind(queue1()).to(exchange()).with(routing_dead_1);
    }

    //  设置第二个队列
    @Bean
    public Queue queue2(){
        //  直接返回
        return new Queue(queue_dead_2,true,false,false,null);
    }

    //  设置第二个绑定关系
    @Bean
    public Binding binding2(){
        //  直接 new 同时也可以采用 BindingBuilder
        return BindingBuilder.bind(queue2()).to(exchange()).with(routing_dead_2);
    }

}
