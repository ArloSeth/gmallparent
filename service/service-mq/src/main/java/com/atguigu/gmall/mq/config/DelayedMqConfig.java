package com.atguigu.gmall.mq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * @author mqx
 * @date 2020-11-21 10:22:24
 */
@Configuration
public class DelayedMqConfig {

    //  声明变量
    public static final String exchange_delay = "exchange.delay";
    public static final String routing_delay = "routing.delay";
    public static final String queue_delay_1 = "queue.delay.1";

    //  制作一个交换机
    @Bean
    public CustomExchange delayExchange(){
        HashMap<String, Object> map = new HashMap<>();
        //  设置交换机参数
        map.put("x-delayed-type","direct");
        //  直接new
        return new CustomExchange(exchange_delay,"x-delayed-message",true,false,map);
    }
    //  制作一个队列
    @Bean
    public Queue delayQeue1(){
        //  直接返回队列
        return new Queue(queue_delay_1,true,false,false,null);
    }
    //  设置绑定关系
    @Bean
    public Binding delayBinding1(){
        //  返回设置
        return BindingBuilder.bind(delayQeue1()).to(delayExchange()).with(routing_delay).noargs();
    }



}
