package com.atguigu.gmall.product.service;

/**
 * @author mqx
 * @date 2020-11-9 14:40:44
 */
public interface TestService {

    //  测试锁
    void testLock();
    //  读锁
    String readLock();

    //  写锁
    String writeLock();
}
