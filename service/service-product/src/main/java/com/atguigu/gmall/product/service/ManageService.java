package com.atguigu.gmall.product.service;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.model.product.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jdk.nashorn.internal.ir.LiteralNode;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author mqx
 * 业务层接口
 * @date 2020-11-3 10:31:33
 */
public interface ManageService {

    // 查询所有一级分类数据
    List<BaseCategory1> getCategory1();

    // 根据一级分类Id 查询二级分类数据
    List<BaseCategory2> getCategory2(Long category1Id);

    //  根据二级分类Id 查询三级分类数据
    List<BaseCategory3> getCategory3(Long category2Id);

    //  获取到平台属性数据集合
    List<BaseAttrInfo> getAttrInfoList(Long category1Id,
                                       Long category2Id,
                                       Long category3Id);


    //  保存平台属性数据
    void saveAttrInfo(BaseAttrInfo baseAttrInfo);

    //  根据属性Id 查询平台属性值集合数据
    List<BaseAttrValue> getAttrValueList(Long attrId);

    //  根据平台属性Id 查询平台属性对象
    BaseAttrInfo getBaseAttrInfo(Long attrId);

    //  按照三级分类Id带分页的查询?
    //  IPage<SpuInfo> getSpuInfoPage(Page<SpuInfo> page, Long category3Id);

    //  springmvc 对象传值  按照三级分类Id带分页的查询
    IPage<SpuInfo> getSpuInfoPage(Page<SpuInfo> page, SpuInfo spuInfo);

    //  查询所有的销售属性数据
    List<BaseSaleAttr> getBaseSaleAttrList();

    //  保存spuInfo
    void saveSpuInfo(SpuInfo spuInfo);

    // 根据spuId 查询spuImage 集合。
    List<SpuImage> getSpuImageList(Long spuId);

    //  根据spuId 查询SpuSaleAttr集合。
    List<SpuSaleAttr> getSpuSaleAttrList(Long spuId);

    //  保存skuInfo
    void saveSkuInfo(SkuInfo skuInfo);

    //  带分页查询skuInfo 列表
    IPage<SkuInfo> getPage(Page<SkuInfo> page);

    //  商品上架
    void onSale(Long skuId);

    //  商品下架
    void cancelSale(Long skuId);

    //  根据skuId 获取数据
    SkuInfo getSkuInfo(Long skuId);

    //  根据三级分类Id 查询分类名称
    BaseCategoryView getCategoryViewByCategory3Id(Long category3Id);

    //  根据skuId 获取实时价格！
    BigDecimal getSkuPrice(Long skuId);

    //  根据skuId, spuId 获取销售属性集合数据
    List<SpuSaleAttr> getSpuSaleAttrListCheckBySku(Long skuId, Long spuId);

    //  根据spuId 获取销售属性值Id 与 skuId 组成的数据！ 先定义成map 最后转成Json
    Map getSkuValueIdsMap(Long spuId);

    //  封装首页数据 JSONObject 存储数据 本质是map！
    List<JSONObject> getBaseCategoryList();
    //  map 与 JSONObject ,JSONObject 属于JSON 的子类，但是 map 不是！
    // List<Map> getxxx();

    /**
     * 通过品牌Id 来查询数据
     * @param tmId
     * @return
     */
    BaseTrademark getTrademarkByTmId(Long tmId);

    /**
     * 通过skuId 集合来查询数据
     * @param skuId
     * @return
     */
    List<BaseAttrInfo> getAttrList(Long skuId);



}
