package com.atguigu.gmall.product.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.common.cache.GmallCache;
import com.atguigu.gmall.common.constant.MqConst;
import com.atguigu.gmall.common.constant.RedisConst;
import com.atguigu.gmall.common.service.RabbitService;
import com.atguigu.gmall.model.product.*;
import com.atguigu.gmall.product.mapper.*;
import com.atguigu.gmall.product.service.ManageService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author mqx
 * @date 2020-11-3 10:35:34
 */
@Service
public class ManageServiceImpl implements ManageService{

    //  注入mapper
    @Autowired
    private BaseCategory1Mapper baseCategory1Mapper;

    @Autowired
    private BaseCategory2Mapper baseCategory2Mapper;

    @Autowired
    private BaseCategory3Mapper baseCategory3Mapper;

    @Autowired
    private BaseAttrInfoMapper baseAttrInfoMapper;

    @Autowired
    private BaseAttrValueMapper baseAttrValueMapper;

    @Autowired
    private SpuInfoMapper spuInfoMapper;

    @Autowired
    private BaseSaleAttrMapper baseSaleAttrMapper;

    @Autowired
    private SpuImageMapper spuImageMapper;

    @Autowired
    private SpuSaleAttrMapper spuSaleAttrMapper;

    @Autowired
    private SpuSaleAttrValueMapper spuSaleAttrValueMapper;

    @Autowired
    private SkuInfoMapper skuInfoMapper;

    @Autowired
    private SkuImageMapper skuImageMapper;

    @Autowired
    private SkuSaleAttrValueMapper skuSaleAttrValueMapper;

    @Autowired
    private SkuAttrValueMapper skuAttrValueMapper;

    @Autowired
    private BaseCategoryViewMapper baseCategoryViewMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private BaseTrademarkMapper baseTrademarkMapper;

    @Autowired
    private RabbitService rabbitService;

    @Override
    public List<BaseCategory1> getCategory1() {
        //  select * from base_category1
        return baseCategory1Mapper.selectList(null);
    }

    @Override
    public List<BaseCategory2> getCategory2(Long category1Id) {
        //  select * from base_category2 where category1_id = category1Id;
        //  查询哪个实体类或者数据库表，
        //  构建查询条件
        QueryWrapper<BaseCategory2> baseCategory2QueryWrapper = new QueryWrapper<>();
        baseCategory2QueryWrapper.eq("category1_id",category1Id);
        return baseCategory2Mapper.selectList(baseCategory2QueryWrapper);
    }

    @Override
    public List<BaseCategory3> getCategory3(Long category2Id) {
        //  select * from base_category3 where category2_id = category2Id;
        return baseCategory3Mapper.selectList(new QueryWrapper<BaseCategory3>().eq("category2_id",category2Id));
    }

    /**
     * sku01:   Apple iPhone 11 (A2223) 128GB 黑色 移动联通电信4G手机 双卡双待
     *          当前这个商品 有个平台属性 操作系统 有个属性值 IOS
     *
     *
     * 页面根据分类Id 查询平台属性，没有属性值
     * 但是，需要通过这个方法将平台属性，以及平台属性值一起查出来！
     * 提示：在制作sku 的时候，我们需要选择平台属性，以及平台属性的值！
     * @param category1Id
     * @param category2Id
     * @param category3Id
     * @return
     */
    @Override
    public List<BaseAttrInfo> getAttrInfoList(Long category1Id, Long category2Id, Long category3Id) {
        //  select * from base_attr_info where category_id = ?category1Id  category2Id category3Id
        //  判断 ： mybatis 动态sql if ()
        return baseAttrInfoMapper.selectBaseAttrInfoList(category1Id,category2Id,category3Id);
    }

    @Override
    @Transactional
    public void saveAttrInfo(BaseAttrInfo baseAttrInfo) {
        /*
          base_attr_info
          base_attr_value
         */
        //  什么时候是修改，什么时候是保存?
        if (baseAttrInfo.getId()!=null){
            //  id 是主键
            //  update base_attr_info set attr_name = baseAttrInfo.getAttrName() where id = baseAttrInfo.getId();
            baseAttrInfoMapper.updateById(baseAttrInfo);
        }else {
            //  保存！
            //  baseAttrInfo.getId() = null;
            //  base_attr_info insert(); 当执行完成insert 之后，那么id 会自动获取到！
            baseAttrInfoMapper.insert(baseAttrInfo);
        }
        //  先delete,跟attrId 相关的属性值，再insert
        //  delete from base_attr_value where attr_id=baseAttrInfo.getId();
        baseAttrValueMapper.delete(new QueryWrapper<BaseAttrValue>().eq("attr_id",baseAttrInfo.getId()));

        //  int i = 1/0;
        //   baseAttrInfo.getId() !=null;
        //  base_attr_value
        List<BaseAttrValue> attrValueList = baseAttrInfo.getAttrValueList();
        //  判断集合不为空！
        if (!CollectionUtils.isEmpty(attrValueList)){
            //  循环遍历
            for (BaseAttrValue baseAttrValue : attrValueList) {
                //  插入数据!
                //  因为前台传递数据的时候，并没有将attr_id 传递，但是我们可以通过业务指的当前的数据是谁?
                //  attr_id = baseAttrInfo.getId();
                baseAttrValue.setAttrId(baseAttrInfo.getId());
                baseAttrValueMapper.insert(baseAttrValue);
            }
        }
    }

    @Override
    public List<BaseAttrValue> getAttrValueList(Long attrId) {
        //  select * from base_attr_value where attr_id =  attrId;
        return baseAttrValueMapper.selectList(new QueryWrapper<BaseAttrValue>().eq("attr_id",attrId));
    }

    @Override
    public BaseAttrInfo getBaseAttrInfo(Long attrId) {
        //  attrId = base_attr_value.attr_id = base_attr_info.id
        BaseAttrInfo baseAttrInfo = baseAttrInfoMapper.selectById(attrId);
        //  判断平台属性不为空！
        if (baseAttrInfo!=null){
            //  baseAttrInfo 没有属性值集合字段
            //  给attrValueList 赋值！因为控制器要调用平台属性值集合！
            //  select * from base_attr_value where attr_id =  attrId;
            baseAttrInfo.setAttrValueList(this.getAttrValueList(attrId));
        }
        //  返回数据！
        return baseAttrInfo;
    }

    @Override
    public IPage<SpuInfo> getSpuInfoPage(Page<SpuInfo> page, SpuInfo spuInfo) {

        //  构建查询条件
        //  mysql 默认排序方法 降序！
        QueryWrapper<SpuInfo> spuInfoQueryWrapper = new QueryWrapper<>();
        spuInfoQueryWrapper.eq("category3_id",spuInfo.getCategory3Id());
        spuInfoQueryWrapper.orderByDesc("id");
        return spuInfoMapper.selectPage(page,spuInfoQueryWrapper);
    }

    @Override
    public List<BaseSaleAttr> getBaseSaleAttrList() {
        return baseSaleAttrMapper.selectList(null);
    }

    @Override
    @Transactional
    public void saveSpuInfo(SpuInfo spuInfo) {
        /*
        spuInfo
        spuImage
        spuSaleAttr
        spuSaleAttrValue
         */
        spuInfoMapper.insert(spuInfo);

        //  获取spuImageList 集合数据
        List<SpuImage> spuImageList = spuInfo.getSpuImageList();
        //  判断
        if (!CollectionUtils.isEmpty(spuImageList)){
            //  循环遍历
            for (SpuImage spuImage : spuImageList) {
                //  保存数据
                spuImage.setSpuId(spuInfo.getId());
                spuImageMapper.insert(spuImage);
            }
        }
        //  获取销售属性集合
        List<SpuSaleAttr> spuSaleAttrList = spuInfo.getSpuSaleAttrList();
        if (!CollectionUtils.isEmpty(spuSaleAttrList)){
            //  循环遍历
            for (SpuSaleAttr spuSaleAttr : spuSaleAttrList) {
                spuSaleAttr.setSpuId(spuInfo.getId());
                spuSaleAttrMapper.insert(spuSaleAttr);

                //  获取到销售属性值集合对象
                //  spuSaleAttrValue
                List<SpuSaleAttrValue> spuSaleAttrValueList = spuSaleAttr.getSpuSaleAttrValueList();
                //  判断
                if (!CollectionUtils.isEmpty(spuSaleAttrValueList)){
                    for (SpuSaleAttrValue spuSaleAttrValue : spuSaleAttrValueList) {
                        //  数据补全！
                        spuSaleAttrValue.setSpuId(spuInfo.getId());
                        spuSaleAttrValue.setSaleAttrName(spuSaleAttr.getSaleAttrName());
                        spuSaleAttrValueMapper.insert(spuSaleAttrValue);
                    }
                }
            }
        }
    }

    @Override
    public List<SpuImage> getSpuImageList(Long spuId) {
        //  select  * from spu_image where spu_id = 17
        QueryWrapper<SpuImage> spuImageQueryWrapper = new QueryWrapper<>();
        spuImageQueryWrapper.eq("spu_id",spuId);
        return spuImageMapper.selectList(spuImageQueryWrapper);
    }

    @Override
    public List<SpuSaleAttr> getSpuSaleAttrList(Long spuId) {
        //  sql 语句
        return spuSaleAttrMapper.selectSpuSaleAttrList(spuId);
    }

    //  skuInfo
    @Override
    @Transactional
    public void saveSkuInfo(SkuInfo skuInfo) {
        /*
            skuInfo,skuAtrrValue,skuSaleAttrValue,skuImage
         */

        //  skuInfo
        skuInfoMapper.insert(skuInfo);
        //  skuImage
        List<SkuImage> skuImageList = skuInfo.getSkuImageList();
        if (!CollectionUtils.isEmpty(skuImageList)){
            // 循环遍历
            for (SkuImage skuImage : skuImageList) {
                //  数据补全
                skuImage.setSkuId(skuInfo.getId());
                skuImageMapper.insert(skuImage);
            }
        }
        //  skuAtrrValue
        List<SkuAttrValue> skuAttrValueList = skuInfo.getSkuAttrValueList();
        if (!CollectionUtils.isEmpty(skuAttrValueList)){
            //  循环遍历
            for (SkuAttrValue skuAttrValue : skuAttrValueList) {
                //  数据补全
                skuAttrValue.setSkuId(skuInfo.getId());
                skuAttrValueMapper.insert(skuAttrValue);
            }
        }
        //  skuSaleAttrValue
        List<SkuSaleAttrValue> skuSaleAttrValueList = skuInfo.getSkuSaleAttrValueList();
        if (!CollectionUtils.isEmpty(skuSaleAttrValueList)){
            for (SkuSaleAttrValue skuSaleAttrValue : skuSaleAttrValueList) {

                //  数据补全
                skuSaleAttrValue.setSkuId(skuInfo.getId());
                skuSaleAttrValue.setSpuId(skuInfo.getSpuId());
                skuSaleAttrValueMapper.insert(skuSaleAttrValue);
            }
        }
        //  保存完成之后，进行商品上架
        rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_GOODS,MqConst.ROUTING_GOODS_UPPER,skuInfo.getId());
    }

    @Override
    public IPage<SkuInfo> getPage(Page<SkuInfo> page) {
        QueryWrapper<SkuInfo> skuInfoQueryWrapper = new QueryWrapper<>();
        skuInfoQueryWrapper.orderByDesc("id");
        return skuInfoMapper.selectPage(page,skuInfoQueryWrapper);
    }

    @Override
    public void onSale(Long skuId) {
        //  update sku_info set is_sale = 1 where id = skuId
        SkuInfo skuInfo = new SkuInfo();
        skuInfo.setId(skuId);
        skuInfo.setIsSale(1);
        skuInfoMapper.updateById(skuInfo);

        //  上架 自动完成es 中 上架  localhost:8203/api/list/inner/upperGoods/33
        //  远程调用 service-list.upperGoods(skuId);
        //  发送一个消息队列 执行 upperGoods
        //  发送的内容要根据消费者做什么处理来决定么?
        rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_GOODS,MqConst.ROUTING_GOODS_UPPER,skuId);

    }

    @Override
    public void cancelSale(Long skuId) {
        //  update sku_info set is_sale = 0 where id=skuId
        SkuInfo skuInfo = new SkuInfo();
        skuInfo.setId(skuId);
        skuInfo.setIsSale(0);
        skuInfoMapper.updateById(skuInfo);

        //  下架：自动完成es 中 下架  localhost:8203/api/list/inner/lowerGoods/33
        //  lowerGoods(skuId)
        //  发送一个消息队列 执行 lowerGoods

        rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_GOODS,MqConst.ROUTING_GOODS_LOWER,skuId);
    }

    @Override
    @GmallCache(prefix = "sku:") // 实现分布式锁
    public SkuInfo getSkuInfo(Long skuId) {
        /*
        if(true){
            getRedis
        }else{
            getDB
            setRedis
        }
         */
        //  return getSkuInfoByRedis(skuId);
        return getSkuInfoDB(skuId);
        //return getSkuInfoByRedisson(skuId);
    }

    private SkuInfo getSkuInfoByRedisson(Long skuId) {
        SkuInfo skuInfo = null;
        try {
            //  skuKey = sku:skuId:info set(skuKey,skuInfo)
            String skuKey = RedisConst.SKUKEY_PREFIX+skuId+RedisConst.SKUKEY_SUFFIX;
            //  获取缓存中的数据
            skuInfo = (SkuInfo) redisTemplate.opsForValue().get(skuKey);
            //  判断缓存中是否存在数据
            if (skuInfo==null){
                //  做分布式锁
                //  lockSkuKey=sku:skuId:lock
                String lockSkuKey = RedisConst.SKUKEY_PREFIX+skuId+RedisConst.SKULOCK_SUFFIX;
                RLock lock = redissonClient.getLock(lockSkuKey);
                //  调用方法上锁
                boolean res = lock.tryLock(RedisConst.SKULOCK_EXPIRE_PX1, RedisConst.SKULOCK_EXPIRE_PX2, TimeUnit.SECONDS);
                //  表示上锁成功
                if (res){
                    try {
                        //  从数据库中获取数据，并放入缓存
                        skuInfo =  getSkuInfoDB(skuId);
                        //  防止缓存穿透
                        if (skuInfo==null){
                            SkuInfo skuInfo1 = new SkuInfo();
                            //  放入缓存
                            redisTemplate.opsForValue().set(skuKey,skuInfo1,RedisConst.SKUKEY_TEMPORARY_TIMEOUT,TimeUnit.SECONDS);
                            //  放回数据
                            return skuInfo1;
                        }

                        redisTemplate.opsForValue().set(skuKey,skuInfo,RedisConst.SKUKEY_TIMEOUT,TimeUnit.SECONDS);
                        //  返回数据
                        return skuInfo;
                    } finally {
                        lock.unlock();
                    }
                }else {
                    //  等待睡觉
                    try {
                        Thread.sleep(1000);
                        //  自旋
                        return getSkuInfo(skuId);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }else {
                return skuInfo;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //  数据库兜底！
        return getSkuInfoDB(skuId);
    }

    //  利用redis-set 命令来实现分布式锁！
    private SkuInfo getSkuInfoByRedis(Long skuId) {
        SkuInfo skuInfo = null;
        try {
            //  获取缓存中的数据?   数据类型 {String}，key 如何起名 Jedis {set(String ,String)}
            //  skuKey = sku:skuId:info set(skuKey,skuInfo)
            String skuKey = RedisConst.SKUKEY_PREFIX+skuId+RedisConst.SKUKEY_SUFFIX;
            //  获取缓存中的数据
            skuInfo = (SkuInfo) redisTemplate.opsForValue().get(skuKey);
            //  判断缓存中是否存在数据
            if (skuInfo==null){
                //  int i = 1/0;
                //  缓存没有数据，那么就要查询数据了！考虑添加锁！防止缓存击穿
                //  定义分布式锁的key
                //  lockSkuKey=sku:skuId:lock
                String lockSkuKey = RedisConst.SKUKEY_PREFIX+skuId+RedisConst.SKULOCK_SUFFIX;
                //  给每个锁设置一个随机值
                String uuid = UUID.randomUUID().toString();
                //  准备上锁    set sku:skuId:lock uuid ex 1 nx
                Boolean flag = redisTemplate.opsForValue().setIfAbsent(lockSkuKey, uuid,RedisConst.SKULOCK_EXPIRE_PX1, TimeUnit.SECONDS);
                //  表示上锁成功
                if (flag){
                    System.out.println("获取到了分布式锁！");
                    //  开始你的表演 查询数据，并将数据放入缓存
                    skuInfo =  getSkuInfoDB(skuId);
                    //  防止缓存穿透
                    if (skuInfo==null){
                        SkuInfo skuInfo1 = new SkuInfo();
                        //  放入缓存
                        redisTemplate.opsForValue().set(skuKey,skuInfo1,RedisConst.SKUKEY_TEMPORARY_TIMEOUT,TimeUnit.SECONDS);
                        //  放回数据
                        return skuInfo1;
                    }

                    //  缓存中存储商品详情的时候，我们需要有个过期时间
                    redisTemplate.opsForValue().set(skuKey,skuInfo,RedisConst.SKUKEY_TIMEOUT,TimeUnit.SECONDS);

                    //  删除锁！使用lua 脚本
                    String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";

                    //  执行lua 脚本
                    DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
                    redisScript.setScriptText(script);
                    redisScript.setResultType(Long.class);

                    redisTemplate.execute(redisScript, Arrays.asList(lockSkuKey),uuid);
                    //  返回数据
                    return skuInfo;
                }else {
                    //  等待睡觉
                    try {
                        Thread.sleep(1000);
                        //  自旋
                        return getSkuInfo(skuId);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }else {
                //  返回数据
                return skuInfo;
            }
        } catch (Exception e) {
            //  获取到异常信息！
            e.printStackTrace();
        }
        return getSkuInfoDB(skuId);
    }

    //  将代码提取出来，变成根据skuId 获取skuInfo 数据！
    private SkuInfo getSkuInfoDB(Long skuId) {
        //  select * from sku_info where id = skuId;
        SkuInfo skuInfo = skuInfoMapper.selectById(skuId);

        if (skuInfo!=null){
            //  获取skuImage 数据
            //  select * from sku_image where sku_id = skuId
            List<SkuImage> skuImageList = skuImageMapper.selectList(new QueryWrapper<SkuImage>().eq("sku_id", skuId));
            skuInfo.setSkuImageList(skuImageList);
        }

        //  将数据返回
        return skuInfo;
    }

    @Override
    @GmallCache(prefix = "categoryView:")
    public BaseCategoryView getCategoryViewByCategory3Id(Long category3Id) {

        //  select * from base_category_view where id = 61;
        return baseCategoryViewMapper.selectById(category3Id);
    }

    @Override
    @GmallCache(prefix = "skuPrice:")
    public BigDecimal getSkuPrice(Long skuId) {
        //  只要价格
        SkuInfo skuInfo = skuInfoMapper.selectById(skuId);
        if(skuInfo!=null){
            return skuInfo.getPrice();
        }
        return new BigDecimal(0);
    }

    @Override
    @GmallCache(prefix = "spuSaleAttrListCheckBySku:")
    public List<SpuSaleAttr> getSpuSaleAttrListCheckBySku(Long skuId, Long spuId) {
        //  调用mapper
        return spuSaleAttrMapper.selectSpuSaleAttrListCheckBySku(skuId,spuId);
    }

    @Override
    @GmallCache(prefix = "skuValueIdsMap:")
    public Map getSkuValueIdsMap(Long spuId) {
        HashMap<Object,Object> hashMap = new HashMap<>();
        //  map 中存储什么数据? map.put("75|78","33");  map.put("75|77","32");
        //  调用mapper 获取数据 75|78 skuId = 33 75|77 skuId = 32
        //  自定义DTO
        /*
        class DtoMaps{
            private Long skuId;
            private String valuesId;

        }
        可以使用map 代替实体类：new DtoMaps().setSkuId(32);
         map.put(key,value) key = skuId  map.put(skuId,32)  map.put(valuesId,75|77)
         key=valuesId
         */
        List<Map> mapList = skuSaleAttrValueMapper.selectSaleAttrValuesBySpu(spuId);
        // 循环 遍历mapList 获取数据
        if(!CollectionUtils.isEmpty(mapList)){
            for (Map map : mapList) {
                // Object object = map.get("sku_id");
                //  map.put("75|78","33");
                hashMap.put(map.get("values_id"),map.get("sku_id"));
            }
        }

        return hashMap;
    }

    @Override
    @GmallCache(prefix = "index:")
    public List<JSONObject> getBaseCategoryList() {
        //  存储json 数据
        List<JSONObject> list = new ArrayList<>();
        //  分类数据都在base_category_view 整个视图中！
        List<BaseCategoryView> baseCategoryViewList = baseCategoryViewMapper.selectList(null);
        //  按照组成json 字符串的规则进行拼接
        //  key = category1Id vlaue = List<BaseCategoryView>
        Map<Long, List<BaseCategoryView>> category1Map = baseCategoryViewList.stream().collect(Collectors.groupingBy(BaseCategoryView::getCategory1Id));

        //  声明一个index
        int index = 1;

        //  拼接json 字符串 ，循环遍历集合
        for (Map.Entry<Long, List<BaseCategoryView>> entry : category1Map.entrySet()) {
            //  获取一级分类Id
            Long category1Id = entry.getKey();
            //  获取一级分类Id 所对应的value
            List<BaseCategoryView> category2List = entry.getValue();
            //  创建一个对象
            JSONObject category1 = new JSONObject();

            //  存储一级分类Id
            category1.put("index",index);
            category1.put("categoryId",category1Id);
            category1.put("categoryName",category2List.get(0).getCategory1Name());
            //  category1.put("categoryChild","");
            //  变量迭代
            index++;
            //  处理二级分类数据
            //  key=category2Id value=List<BaseCategoryView>
            Map<Long, List<BaseCategoryView>> category2Map = category2List.stream().collect(Collectors.groupingBy(BaseCategoryView::getCategory2Id));
            //  声明一个存储二级分类JSONObject集合
            List<JSONObject> category2Child = new ArrayList<>();

            //  循环遍历
            for (Map.Entry<Long, List<BaseCategoryView>> entry1 : category2Map.entrySet()) {
                //  获取category2Id 分类Id
                Long category2Id = entry1.getKey();
                //  获取value
                List<BaseCategoryView> category3List = entry1.getValue();
                //  创建一个对象
                JSONObject category2 = new JSONObject();
                category2.put("categoryId",category2Id);
                category2.put("categoryName",category3List.get(0).getCategory2Name());
                //  将每个二级分类的数据添加到集合中！
                category2Child.add(category2);

                //  声明三级分类的集合
                List<JSONObject> category3Child = new ArrayList<>();
                //  获取三级分类数据
                category3List.forEach(baseCategoryView -> {
                    //  创建一个对象
                    JSONObject category3 = new JSONObject();
                    category3.put("categoryId",baseCategoryView.getCategory3Id());
                    category3.put("categoryName",baseCategoryView.getCategory3Name());
                    category3Child.add(category3);
                });
                //  将三级分类数据集合添加到二级分类
                category2.put("categoryChild",category3Child);
            }
            //  将二级分类数据添加到一级中
            category1.put("categoryChild",category2Child);
            //  将一级分类数据添加到list 中！
            list.add(category1);
        }
        //  返回
        return list;
    }

    @Override
    public BaseTrademark getTrademarkByTmId(Long tmId) {
        return baseTrademarkMapper.selectById(tmId);
    }

    @Override
    public List<BaseAttrInfo> getAttrList(Long skuId) {

        // 多表关联查询
        return baseAttrInfoMapper.selectAttrList(skuId);
    }

}
