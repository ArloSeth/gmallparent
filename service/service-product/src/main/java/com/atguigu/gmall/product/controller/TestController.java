package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author mqx
 * @date 2020-11-9 14:39:51
 */
@RestController
@RequestMapping("admin/product/test")
public class TestController {

    @Autowired
    private TestService testService;
    //  控制器
    @GetMapping("testLock")
    public Result testLock(){
        testService.testLock();
        return Result.ok();
    }

    //  读锁
    @GetMapping("read")
    public Result readLock(){
        //  读取到的数据
        String msg = testService.readLock();
        //  读取到的数据写入result
        return Result.ok(msg);
    }

    //  写锁
    @GetMapping("write")
    public Result writeLock(){
        //  读取到的数据
        String msg = testService.writeLock();
        //  读取到的数据写入result
        return Result.ok(msg);
    }
}
