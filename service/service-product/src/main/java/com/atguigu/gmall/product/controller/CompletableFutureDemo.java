package com.atguigu.gmall.product.controller;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author mqx
 * @date 2020-11-11 10:39:04
 */
public class CompletableFutureDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //  supplyAsync 创建对象方法有返回值
        //  whenComplete 获取计算结果
        //  exceptionally 表示获取异常结果
        //  Supplier 没有参数有返回值
        //  BiConsumer  有参数无返回值
//        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(new Supplier<Integer>() {
//            @Override
//            public Integer get() {
//                System.out.println("CompletableFuture ---- come on!");
//                //  int i = 1/0;
//                return 1024;
//            }
//        }).thenApply(new Function<Integer, Integer>() {
//            @Override
//            public Integer apply(Integer integer) {
//                System.out.println("integer----------"+integer);
//
//                return integer*2;
//            }
//        }).whenComplete(new BiConsumer<Integer, Throwable>() {
//            @Override
//            public void accept(Integer integer, Throwable throwable) {
//                System.out.println("integer----------"+integer);
//                System.out.println("throwable----------"+throwable);
//            }
//        }).exceptionally(new Function<Throwable, Integer>() {
//            @Override
//            public Integer apply(Throwable throwable) {
//                System.out.println("throwable----------"+throwable);
//                return 404;
//            }
//        });
//        //  CompletableFuture它的get();都是放在最后执行！
//
//        //  future.get();
//        System.out.println(future.get());

        //  第一个线程
        CompletableFuture<String> futureA = CompletableFuture.supplyAsync(() -> {
            return "hello";
        });

        //  第二个线程 使用第一个线程的执行结果拼接 "第二个线程"
        //  Consumer    有参数没有返回值
        CompletableFuture<Void> futureB = futureA.thenAcceptAsync((t) -> {
            //  第一个睡眠
            delaySec(3);
            //  第二个打印数据
            printCurrTime(t+" 第二个线程");
        });

        //  第三个线程 使用第一个线程的执行结果拼接 "第三个线程"
        CompletableFuture<Void> futureC = futureA.thenAcceptAsync((t) -> {
            //  第一个睡眠
            delaySec(1);
            //  第二个打印数据
            printCurrTime(t+" 第三个线程");
        });

        //  调用方法
        futureB.get();
        futureC.get();




    }
    //  打印方法
    private static void printCurrTime(String s) {
        System.out.println(s);
    }

    //  睡眠
    private static void delaySec(int i) {
        try {
            Thread.sleep(i*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
