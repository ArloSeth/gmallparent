package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.model.product.BaseTrademark;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author mqx
 * @date 2020-11-4 14:04:09
 */
@Mapper
public interface BaseTrademarkMapper extends BaseMapper<BaseTrademark> {
}
