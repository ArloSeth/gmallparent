package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.model.product.BaseSaleAttr;
import com.atguigu.gmall.model.product.SpuImage;
import com.atguigu.gmall.model.product.SpuInfo;
import com.atguigu.gmall.model.product.SpuSaleAttr;
import com.atguigu.gmall.product.service.ManageService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author mqx
 * @date 2020-11-4 11:33:56
 */
@Api(tags = "后台SPU数据接口测试")
@RestController // @ReponseBody + @Controller
@RequestMapping("admin/product")
public class SpuManageController {

    @Autowired
    private ManageService manageService;
    // http://api.gmall.com/admin/product/{page}/{limit}?category3Id=61
    // http://api.gmall.com/admin/product/1/10?category3Id=61
    //  @RequestBody 将json 转化为JavaObject .
    //  分页需要pageSize,pageNo
    @GetMapping("{page}/{limit}")
    public Result getSpuList(@PathVariable Long page,
                             @PathVariable Long limit,
                             // @RequestParam("category3Id") Long category3Id,
                             // Long category3Id,
                             // @RequestBody SpuInfo spuInfo,
                             // HttpServletRequest request,
                             SpuInfo spuInfo){

        Page<SpuInfo> spuInfoPage = new Page<>(page,limit);
        IPage<SpuInfo> spuInfoList = manageService.getSpuInfoPage(spuInfoPage, spuInfo);
        return Result.ok(spuInfoList);
    }

    // http://api.gmall.com/admin/product/baseSaleAttrList
    @GetMapping("baseSaleAttrList")
    public Result baseSaleAttrList(){
        //  调用服务层方法
        List<BaseSaleAttr> baseSaleAttrList = manageService.getBaseSaleAttrList();

        return Result.ok(baseSaleAttrList);
    }

    // http://api.gmall.com/admin/product/saveSpuInfo
    //  前台传的Json --->Java Object
    @PostMapping("saveSpuInfo")
    public Result saveSpuInfo(@RequestBody SpuInfo spuInfo){
        //  调用服务层方法
        manageService.saveSpuInfo(spuInfo);

        return Result.ok();
    }

    // http://api.gmall.com/admin/product/spuImageList/{spuId}
    @GetMapping("spuImageList/{spuId}")
    public Result getSpuImageList(@PathVariable Long spuId){
        //  调用服务层
        List<SpuImage> list = manageService.getSpuImageList(spuId);

        return Result.ok(list);

    }

    //  http://api.gmall.com/admin/product/spuSaleAttrList/{spuId}
    @GetMapping("spuSaleAttrList/{spuId}")
    public Result getSpuSaleAttrList(@PathVariable Long spuId){
        //  调用方法
        List<SpuSaleAttr> spuSaleAttrList =  manageService.getSpuSaleAttrList(spuId);
        return Result.ok(spuSaleAttrList);
    }
}
