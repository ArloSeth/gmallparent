package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.BaseTrademark;
import com.atguigu.gmall.model.product.SpuInfo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author mqx
 * @date 2020-11-4 14:05:18
 */
public interface BaseTrademarkService extends IService<BaseTrademark> {

    //  查询所有品牌数据！带分页！
    //  上午做过一个
    //  List<BaseTrademark> selectPage();
    //  业务层中获取数据， get ,mapper 层，我们叫select
    IPage<BaseTrademark> getPage(Page<BaseTrademark> page);

    //  保存品牌
    //  void saveBaseTrademark(BaseTrademark baseTrademark);

}
