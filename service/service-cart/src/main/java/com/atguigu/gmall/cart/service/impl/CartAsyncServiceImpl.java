package com.atguigu.gmall.cart.service.impl;

import com.atguigu.gmall.cart.mapper.CartInfoMapper;
import com.atguigu.gmall.cart.service.CartAsyncService;
import com.atguigu.gmall.model.cart.CartInfo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author mqx
 * @date 2020-11-17 10:10:54
 */
@Service
public class CartAsyncServiceImpl implements CartAsyncService {

    //  操作数据库
    @Autowired
    private CartInfoMapper cartInfoMapper;

    @Override
    @Async
    public void saveCartInfo(CartInfo cartInfo) {
        System.out.println("保存数据");
        cartInfoMapper.insert(cartInfo);
    }

    @Override
    @Async
    public void updateCartInfo(CartInfo cartInfo) {
        System.out.println("更新数据");

        //  cartInfoMapper.updateById(cartInfo);
        QueryWrapper<CartInfo> cartInfoQueryWrapper = new QueryWrapper<>();
        cartInfoQueryWrapper.eq("user_id",cartInfo.getUserId());
        cartInfoQueryWrapper.eq("sku_id",cartInfo.getSkuId());
        //  更新数据
        cartInfoMapper.update(cartInfo,cartInfoQueryWrapper);
    }

    @Override
    @Async
    public void deleteCartInfo(String userId) {
        QueryWrapper<CartInfo> cartInfoQueryWrapper = new QueryWrapper<>();
        cartInfoQueryWrapper.eq("user_id",userId);
        cartInfoMapper.delete(cartInfoQueryWrapper);

    }

    @Override
    @Async
    public void checkCart(String userId, Integer isChecked, Long skuId) {
        CartInfo cartInfo = new CartInfo();
        cartInfo.setIsChecked(isChecked);

        //  赋值更新条件
        QueryWrapper<CartInfo> cartInfoQueryWrapper = new QueryWrapper<>();
        cartInfoQueryWrapper.eq("user_id",userId).eq("sku_id",skuId);
        cartInfoMapper.update(cartInfo,cartInfoQueryWrapper);

    }

    @Override
    @Async
    public void deleteCartInfo(String userId, Long skuId) {
        //  delete from cart_info where user_id = userId and sku_id = skuId;
        //  构建删除条件
        QueryWrapper<CartInfo> cartInfoQueryWrapper = new QueryWrapper<>();

        cartInfoQueryWrapper.eq("sku_id",skuId).eq("user_id",userId);
        cartInfoMapper.delete(cartInfoQueryWrapper);

    }
}
