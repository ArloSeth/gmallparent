package com.atguigu.gmall.cart.controller;

import com.atguigu.gmall.cart.service.CartService;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.util.AuthContextHolder;
import com.atguigu.gmall.model.cart.CartInfo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author mqx
 * @date 2020-11-17 09:15:16
 */
@Api(tags = "添加购物车接口")
@RestController
@RequestMapping("api/cart")
public class CartApiController {

    @Autowired
    private CartService cartService;

    //  添加购物车功能控制器 调用服务层，
    @PostMapping("addToCart/{skuId}/{skuNum}")
    public Result addToCart(@PathVariable Long skuId,
                            @PathVariable Integer skuNum,
                            HttpServletRequest request){

        //  获取登录的用户Id，在网关中有个过滤器 ，在过滤器中获取到了userId,同时将用户Id存储到header 中
        String userId = AuthContextHolder.getUserId(request);
        //  用户添加购物车 ，必须要登录么? 登录可以添加，未登录也可以添加购物车！
        //        if (!StringUtils.isEmpty(userId)){
        //            //  添加购物车
        //            cartService.addToCart(skuId,userId,skuNum);
        //        }
        //        //  获取未登录的临时用户Id
        //        if (StringUtils.isEmpty(userId)){
        //            String userTempId = AuthContextHolder.getUserTempId(request);
        //            //  添加购物车
        //            cartService.addToCart(skuId,userTempId,skuNum);
        //        }

        if (StringUtils.isEmpty(userId)){
            //  获取临时用户Id
            userId = AuthContextHolder.getUserTempId(request);
        }

        //  添加购物车
        cartService.addToCart(skuId,userId,skuNum);
        return Result.ok();
    }

    //  查询购物车列表
    @GetMapping("cartList")
    public Result cartList(HttpServletRequest request){
        //  获取用户Id，临时用户Id
        String userId = AuthContextHolder.getUserId(request);
        String userTempId = AuthContextHolder.getUserTempId(request);

        List<CartInfo> cartList = cartService.getCartList(userId, userTempId);
        //  返回数据
        return Result.ok(cartList);
    }

    //  选中状态变更
    @GetMapping("checkCart/{skuId}/{isChecked}")
    public  Result checkCart(@PathVariable Long skuId,
                             @PathVariable Integer isChecked,
                             HttpServletRequest request) {

        //  获取登录的用户Id
        String userId = AuthContextHolder.getUserId(request);
        //  获取临时用户Id
        if (StringUtils.isEmpty(userId)){
            //  获取临时用户Id
            userId = AuthContextHolder.getUserTempId(request);
        }
        cartService.checkCart(userId,isChecked,skuId);
        //  返回数据
        return Result.ok();
    }

    //  删除方法
    @DeleteMapping("deleteCart/{skuId}")
    public Result deleteCart(@PathVariable Long skuId,
                             HttpServletRequest request){
        //  获取用户Id
        String userId = AuthContextHolder.getUserId(request);
        //  获取临时用户Id
        if (StringUtils.isEmpty(userId)){
            //  获取临时用户Id
            userId = AuthContextHolder.getUserTempId(request);
        }
        cartService.deleteCart(skuId,userId);
        return Result.ok();
    }

    //  获取购物车列表
    @GetMapping("getCartCheckedList/{userId}")
    public List<CartInfo> getCartCheckedList(@PathVariable String userId){
        return cartService.getCartCheckedList(userId);
    }

    //  根据用户Id 更新购物车商品的价格
    @GetMapping("loadCartCache/{userId}")
    public Result loadCartCache(@PathVariable("userId") String userId) {
        cartService.loadCartCache(userId);
        return Result.ok();
    }
}
