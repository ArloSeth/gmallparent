package com.atguigu.gmall.cart.service;

import com.atguigu.gmall.model.cart.CartInfo;

/**
 * @author mqx
 * @date 2020-11-17 10:10:11
 */
public interface CartAsyncService {

    //  保存数据
    void saveCartInfo(CartInfo cartInfo);
    //  修改数据
    void updateCartInfo(CartInfo cartInfo);
    /**
     * 删除
     * @param userId
     */
    void deleteCartInfo(String userId);

    /**
     * 选中状态变更
     * @param userId
     * @param isChecked
     * @param skuId
     */
    void checkCart(String userId, Integer isChecked, Long skuId);

    /**
     * 删除
     * @param userId
     * @param skuId
     */
    void deleteCartInfo(String userId, Long skuId);
}
