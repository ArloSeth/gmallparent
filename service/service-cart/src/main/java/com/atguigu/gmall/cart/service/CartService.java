package com.atguigu.gmall.cart.service;

import com.atguigu.gmall.model.cart.CartInfo;

import java.util.List;

/**
 * @author mqx
 * @date 2020-11-16 15:27:03
 */
public interface CartService {

    //  添加购物车：  参数有谁？skuId，skuNum,userId
    void addToCart(Long skuId, String userId, Integer skuNum);

    /**
     * 通过用户Id 查询购物车列表
     * @param userId 登录的
     * @param userTempId    未登录
     * @return
     */
    List<CartInfo> getCartList(String userId, String userTempId);

    /**
     * 谁的购物车，哪个商品的选中状态！
     * @param userId
     * @param isChecked
     * @param skuId
     */
    void checkCart(String userId, Integer isChecked, Long skuId);

    /**
     * 删除购物车中的某个商品
     * @param skuId
     * @param userId
     */
    void deleteCart(Long skuId, String userId);

    /**
     * 根据用户Id 查询购物车列表
     * @param userId
     * @return
     */
    List<CartInfo> getCartCheckedList(String userId);

    //  更新价格
    List<CartInfo> loadCartCache(String userId);

}
