package com.atguigu.gmall.all.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.order.client.OrderFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author mqx
 * @date 2020-11-18 11:45:18
 */
@Controller
public class OrderController {

    //  获取远程调用对象
    @Autowired
    private OrderFeignClient orderFeignClient;

    //  编写结算控制器
    //  http://order.gmall.com/trade.html
    @GetMapping("trade.html")
    public String trade(Model model){
        //  远程调用
        Result<Map<String, Object>> result = orderFeignClient.trade();
        //  一次全部放入作用域
        model.addAllAttributes(result.getData());
        //  返回页面
        return "order/trade";

    }

}
