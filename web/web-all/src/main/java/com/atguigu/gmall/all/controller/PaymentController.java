package com.atguigu.gmall.all.controller;

import com.atguigu.gmall.model.order.OrderInfo;
import com.atguigu.gmall.order.client.OrderFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author mqx
 * @date 2020-11-21 14:47:59
 */
@Controller
public class PaymentController {

    @Autowired
    private OrderFeignClient orderFeignClient;

    //  http://payment.gmall.com/pay.html?orderId=55
    @GetMapping("pay.html")
    public String pay(Long orderId, HttpServletRequest request){
        // String orderId = request.getParameter("orderId");
        // OrderInfo orderInfo = orderFeignClient.getOrderInfo(Long.parseLong(orderId));
        OrderInfo orderInfo = orderFeignClient.getOrderInfo(orderId);
        //  保存数据！
        request.setAttribute("orderInfo",orderInfo);
        return "payment/pay";
    }

    // 编写回调地址，给用户显示的页面！
    @GetMapping("pay/success.html")
    public String success() {
        return "payment/success";
    }
}
