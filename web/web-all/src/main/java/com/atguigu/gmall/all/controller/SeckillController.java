package com.atguigu.gmall.all.controller;

import com.atguigu.gmall.activity.client.ActivityFeignClient;
import com.atguigu.gmall.common.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;

/**
 * @author mqx
 * @date 2020-11-25 11:19:46
 */
@Controller
public class SeckillController {

    @Autowired
    private ActivityFeignClient activityFeignClient;

    //  秒杀列表
    @GetMapping("seckill.html")
    public String seckillList(Model model){
        Result result = activityFeignClient.findAll();
        //  页面渲染需要后台存储一个list
        model.addAttribute("list",result.getData());
        return "seckill/index";
    }

    //  封装获取秒杀详情页面
    @GetMapping("seckill/{skuId}.html")
    public String seckillItem(@PathVariable Long skuId,Model model){
        Result result = activityFeignClient.getSeckillGoods(skuId);
        //  页面渲染需要后台存储一个item
        model.addAttribute("item",result.getData());
        //  秒杀商品详情页面是谁
        return "seckill/item";
    }

    //  编写控制器来到排队页面！
    //  window.location.href = '/seckill/queue.html?skuId='+this.skuId+'&skuIdStr='+skuIdStr
    @GetMapping("seckill/queue.html")
    public String seckillQueue( HttpServletRequest request){

        String skuId = request.getParameter("skuId");
        String skuIdStr = request.getParameter("skuIdStr");
        System.out.println(skuId+"\t"+skuIdStr);
        //  保存数据
        request.setAttribute("skuId",skuId);
        request.setAttribute("skuIdStr",skuIdStr);

        return "seckill/queue";
    }
}
