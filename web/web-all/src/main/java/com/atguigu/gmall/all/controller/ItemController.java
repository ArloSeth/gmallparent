package com.atguigu.gmall.all.controller;

import com.atguigu.gmall.client.ItemFeignClient;
import com.atguigu.gmall.common.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author mqx
 * @date 2020-11-9 10:42:15
 */
//@RestController  // @ResponseBody + @Controller
@Controller
public class ItemController {

    @Autowired
    private ItemFeignClient itemFeignClient;

    //  这个控制器怎么写?
    @RequestMapping("{skuId}.html")
    public String getItem(@PathVariable Long skuId, HttpServletRequest request, Model model){
        //  调用service-item
        Result<Map> result = itemFeignClient.getItem(skuId);
        // 后台存储 categoryView,skuInfo,price... 这些数据都被我们存储在service-item 的实现类
        // request.setAttribute("key","value");
        //  result.getData() 返回的map ，又因为map 中已经存储好了，key--value 也就是页面需要的key，value!
        model.addAllAttributes(result.getData());

        return "item/index";
    }
}
