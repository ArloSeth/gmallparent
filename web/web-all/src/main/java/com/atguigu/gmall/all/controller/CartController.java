package com.atguigu.gmall.all.controller;

import com.atguigu.gmall.cart.client.CartFeignClient;
import com.atguigu.gmall.model.product.SkuInfo;
import com.atguigu.gmall.product.client.ProductFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author mqx
 * @date 2020-11-18 09:05:42
 */
@Controller
public class CartController {

    @Autowired
    private CartFeignClient cartFeignClient;

    @Autowired
    private ProductFeignClient productFeignClient;
    //  http://cart.gmall.com/addCart.html?skuId=30&skuNum=1
    /*
    当数据传递的时候是{skuId} {skuNum}
    @PathVariable Long skuId,
    @PathVariable Integer skuNum,

     */
    @RequestMapping("addCart.html")
    public String addCart(Long skuId,
                          Integer skuNum,
                          HttpServletRequest request){
//        String skuId = request.getParameter("skuId");
//        String skuNum = request.getParameter("skuNum");

        System.out.println(skuId+"\t"+skuNum);
        //  远程调用
        //  cartFeignClient.addToCart(Long.parseLong(skuId),Integer.parseInt(skuNum));
        cartFeignClient.addToCart(skuId,skuNum);
        //  获取skuInfo
        //  SkuInfo skuInfo = productFeignClient.getSkuInfo(Long.parseLong(skuId));
          SkuInfo skuInfo = productFeignClient.getSkuInfo(skuId);
        //  保存数据到作用域给前台页面使用
        request.setAttribute("skuNum",skuNum);
        request.setAttribute("skuInfo",skuInfo);
        //  返回视图页面
        return "cart/addCart";
    }

    //  展示购物车
    @RequestMapping("cart.html")
    public String cartList(){
        //  返回购物车列表
        return "cart/index";
    }

}
